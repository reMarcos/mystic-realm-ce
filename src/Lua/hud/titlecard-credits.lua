/*
    titlecard credits for music and level

    (C) 2022 by K. "ashifolfi" J.
*/

local function hud_tcard_credits(v, p, ticker, endticker)
    if ticker == endticker then return end
    -- Level design credits
	if not marathonmode then
		if mapheaderinfo[gamemap].lcredits == nil then
			v.drawString(320, 190, "Unavailable", V_SNAPTOBOTTOM|V_SNAPTORIGHT|V_ALLOWLOWERCASE, "thin-right")
		else
			v.drawString(320, 190, mapheaderinfo[gamemap].lcredits:match('(%w.*[^"])'), V_SNAPTOBOTTOM|V_SNAPTORIGHT|V_ALLOWLOWERCASE, "thin-right")
		end
		--Music credits
		if mapheaderinfo[gamemap].mcredits == nil then
			v.drawString(320, 170, "Unavailable", V_SNAPTOBOTTOM|V_SNAPTORIGHT|V_ALLOWLOWERCASE, "thin-right")
		else
			v.drawString(320, 170, "\25  "..mapheaderinfo[gamemap].mcredits:match('(%w.*[^"])'), V_SNAPTOBOTTOM|V_SNAPTORIGHT|V_ALLOWLOWERCASE, "thin-right")
		end
	 else
		if mapheaderinfo[gamemap].lcredits == nil then
			v.drawString(320, 180, "Unavailable", V_SNAPTOBOTTOM|V_SNAPTORIGHT|V_ALLOWLOWERCASE, "thin-right")
		else
			v.drawString(320, 180, mapheaderinfo[gamemap].lcredits:match('(%w.*[^"])'), V_SNAPTOBOTTOM|V_SNAPTORIGHT|V_ALLOWLOWERCASE, "thin-right")
		end

		-- Music credits
		if mapheaderinfo[gamemap].mcredits == nil then
			v.drawString(320, 160, "Unavailable", V_SNAPTOBOTTOM|V_SNAPTORIGHT|V_ALLOWLOWERCASE, "thin-right")
		else
			v.drawString(320, 160, "\25  "..mapheaderinfo[gamemap].mcredits:match('(%w.*[^"])'), V_SNAPTOBOTTOM|V_SNAPTORIGHT|V_ALLOWLOWERCASE, "thin-right")
		end
	 end
end
hud.add(hud_tcard_credits, "titlecard")
