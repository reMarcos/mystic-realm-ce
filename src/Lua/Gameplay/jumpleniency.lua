--Convenient function to see if we're a custom character, and also doubles as a global Lua toggle.
--Put this check in at the start of every player-affecting Lua script.
--This'll drastically increase compatibility with custom characters and physics, and allow "vanilla" gameplay if desired.

--Simple toggle to turn off Mystic Realm physics at player discretion.
COM_AddCommand("nomrphysics", function(p)
	if (gamestate == GS_LEVEL) and p.valid and p.mrce then
		if p.mrce.dontwantphysics == false then
			p.mrce.dontwantphysics = true
			p.mrce.physics = false
			CONS_Printf(p, "\x83" .. "You've opted out of Mystic Realm's custom Lua behaviour")
		else
			p.mrce.dontwantphysics = false
			p.mrce.physics = true
			CONS_Printf(p, "\x83" .. "You've opted back into Mystic Realm's custom Lua behaviour")
		end
	end
end)

rawset(_G, "IsCustomSkin", function(player)
	if player.mrce and player.mrce.customskin or player.mrce.dontwantphysics then --Either the custom character or the player has opted out of custom stuff!
		player.mrce.physics = false
		return true
	elseif player.mrce then
		player.mrce.physics = true
		return false
	end
end)

--Use it like this at the beginning of each script and we now no longer screw up custom characters, physics mods, etc.
 /*
	if IsCustomSkin(player)
		return
	end
	*/

local function honeycheck(p) -- this makes the if statement for "should we let them jump" into a function for ease of edit
	--local canceljump = false
	if p.honey then
		if p.mo.state == S_PLAY_HONEYBACKFLIP or p.mo.state == S_PLAY_HONEYMELEE1 or p.mo.state == S_PLAY_HONEYMELEE2 then
			return true
		end
	end

	return false
end

local function PreThinkJump(player)
	P_DoJump(player, true);

	-- Prevent using abilities immediately, by removing jump inputs.
	player.cmd.buttons = $1 | BT_JUMP;
	player.pflags = $1 | PF_JUMPDOWN;
end

local function RestoreAbility(player)
	player.secondjump = 0;
	player.pflags = $1 & ~PF_THOKKED;
end

local function RemoveAbility(player)
	player.secondjump = UINT8_MAX;
	player.pflags = $1 | PF_THOKKED;
end

local function JumpLeniencyThink(player)
	if player.mo.skin == "adventuresonic" or player.mrce and player.mrce.physics == false then
		return
	end
	if not (player.mo and player.mo.valid) then
		return;
	end
	local mo = player.mo;

	local latency = 0; -- Default to 0 for EXEs without cmd.latency.
	pcall(function()
		latency = player.cmd.latency;
	end);

	-- Init variables
	if (mo.coyoteTime == nil) then
		mo.coyoteTime = 0;
	end

	if (mo.recoveryWait == nil) then
		mo.recoveryWait = 0;
	end

	if player.mo.state == S_PLAY_SPRING then
		return
	end

	if (player.exiting) then
		-- Can't control anyway, don't need to do this.
		return;
	end

	if player.playerstate == PST_DEAD then
		return;
	end

	local pressedJump = false;
	if (player.cmd.buttons & BT_JUMP) and not (player.pflags & PF_JUMPDOWN) then
		pressedJump = true;
	end

	if (P_PlayerInPain(player) == true) then
		mo.coyoteTime = 0; -- Reset coyote time
		mo.recoveryWait = $1 + 1; -- Increment recovery wait time.

		-- The recovery jump from SA2, where you can jump out of your pain state.
		if (pressedJump == true) then
			local baseRecoveryWait = (2*TICRATE)/3; -- 0.667 seconds of waiting, - your latency.

			if (mo.recoveryWait > baseRecoveryWait - latency) then
				PreThinkJump(player);

				--[[
				RemoveAbility(player);

				-- Reset momentum so you can move a bit.
				player.mo.momx = 0;
				player.mo.momy = 0;
				--]]

				RestoreAbility(player);
			end
		end

		-- Don't do any of the coyote time thinking.
		return;
	end

	-- Reset recovery wait time outside of pain state.
	mo.recoveryWait = 0;

	-- "Coyote time" is how much time you have after leaving the ground where you can jump off.
	-- Many modern platformers do this, especially 3D.
	-- Prevents lots of "the jump didn't jump".
	local baseCoyoteTime = TICRATE/9; -- 0.11 seconds, + your latency.

	-- Check if you're in a state where you would normally be allowed to jump.
	local canJump = false;
	if ((P_IsObjectOnGround(mo) == true) or (P_InQuicksand(mo) == true))
	and (player.powers[pw_carry] == CR_NONE) then
		canJump = true;
	end

	if (player.pflags & PF_JUMPED or honeycheck(player)) then
		-- We jumped. We should not have coyote time.
		mo.coyoteTime = 0;
	elseif (canJump == true) then
		-- Set the coyote time while in a state where you can jump.
		mo.coyoteTime = baseCoyoteTime + latency;
	else
		if (pressedJump == true) and (mo.coyoteTime > 0) then
			-- Pressed jump in a state where you can't jump,
			-- but you have coyote time. So we'll give you a jump anyway!
			PreThinkJump(player);
			RestoreAbility(player);
			mo.coyoteTime = 0;
		end

		if (mo.coyoteTime > 0) then
			-- Reduce coyote timer while in a state where you can't jump.
			mo.coyoteTime = $1 - 1;
		end
	end
end

addHook("PreThinkFrame", function()
	for player in players.iterate do
		if player.spectator then return false end
		if not player.realmo then return false end
		JumpLeniencyThink(player);
	end
end);
