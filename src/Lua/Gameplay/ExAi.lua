addHook('BotTiccmd',function(bot,cmd)
	if gamestate != GS_LEVEL then return end
	if ((mapheaderinfo[gamemap].lvlttl == "Dimension Warp") or (mapheaderinfo[gamemap].lvlttl == "Primordial Abyss")) then
		if not bot.valid then return end
		bmo.momx = 0
		bmo.momy = 0
		bmo.momz = 0
		bmo.reactiontime = -1
		bmo.player.powers[pw_ignorelatch] = 32768
		bmo.flags = $|MF_NOCLIP|MF_NOCLIPTHING|MF_SCENERY|MF_NOTHINK
		bmo.flags2 = $|MF2_DONTDRAW
		bmo.state = S_INVISIBLE
	end
end)