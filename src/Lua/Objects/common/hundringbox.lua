freeslot("MT_HUNDRING_BOX")

if MR_continueready == nil then
	rawset(_G, "MR_continueready", false)
end

local soundcounter = 0
local killbox = false
function A_HundRingBox(actor)
	--repurposing this as the Ultra Ring, in singleplayer if you don't have infinite lives, it will give you a continue, otherwise it will give 100 rings. It will despawn in record attack
	//this is primarily because it is currently impossible to acquire continues in mrce stages without tokens
	if not (actor.target and actor.target.player) then
		return
	end

	local player = actor.target.player
	if not netgame and player.lives != INFLIVES and not gamecomplete then  --can't read save mode directly, however a completed savefile obviously is only possible in save mode
		MR_continueready = true
		S_StartSound(player.mo, sfx_s245)
	else
		P_GivePlayerRings(player, 100)
		if player.skipscrap != nil then
			player.skipscrap = $ + 100
		end
		S_StartSound(player.mo, sfx_kc33)
	end
end

addHook("MobjThinker", function(mobj)
    if not mobj and mobj.valid then return end

	if netgame then return end

    if modeattacking or MR_continueready == true
	and mobj.state != S_BOX_POP2 then
		P_RemoveMobj(mobj)
	end
end, MT_HUNDRING_BOX)

addHook("IntermissionThinker", function()
	local p = consoleplayer
	if netgame then return end
	if modeattacking then return end
	if not MR_continueready then return end
	soundcounter = $ + 1
	if soundcounter == 70 and p.continues < 99 then
		S_StartSound(nil, sfx_s23f)
		p.continues = $ + 1
		MR_continueready = false
		soundcounter = 0
	end
end)

addHook("MobjSpawn", function(m)
	m.sashtries = 2
	m.sashdropodds = 4
end, MT_HUNDRING_BOX)
